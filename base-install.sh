ping -c 1 github.com

timedatectl set-ntp true

cfdisk

mkfs.ext4 /dev/sda1 

mkfs.ext4 /dev/sda2

#mkfs.ext4 /dev/sda3

mount /dev/sda2 /mnt

mkdir -p /mnt/boot 

#mkdir /mnt/home

mount /dev/sda1 /mnt/boot

#mount /dev/sda3 /mnt/home

pacstrap /mnt base base-devel linux linux-firmware vim git intel-ucode

genfstab -U /mnt >> /mnt/etc/fstab

cp base-fx.sh /mnt/base-fx.sh 

chmod +x /mnt/base-fx.sh

arch-chroot /mnt bash base-fx.sh && rm /mnt/base-fx.sh

umount -R /mnt

reboot

